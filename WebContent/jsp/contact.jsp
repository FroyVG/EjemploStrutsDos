<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
	<title>Gestión de Contactos - Struts2 Hibernate Example</title>
</head>
<body>

<h1>Gestión de Contactos</h1>
<s:actionerror/>

<s:form action="add" method="post">
	<s:textfield name="contact.firstName" label="Nombre"/>
	<s:textfield name="contact.lastName" label="Apellido"/>
	<s:textfield name="contact.emailId" label="Email"/>
	<s:textfield name="contact.cellNo" label="No. Celular"/>
	<s:textfield name="contact.website" label="Sitio web"/>
	<s:textfield name="contact.birthDate" label="Fecha de nacimiento"/>
	<s:submit value="Agregar Contacto" align="center"/>
</s:form>


<h2>Contactos</h2>
<table>
<tr>
	<th>Nombre</th>
	<th>Email</th>
	<th>No. Celular</th>
	<th>Fecha de Nacimiento</th>
	<th>Sitio web</th>
	<th>Borrar</th>
</tr>
<s:iterator value="contactList" var="contact">
	<tr>
		<td><s:property value="lastName"/>, <s:property value="firstName"/> </td>
		<td><s:property value="emailId"/></td>
		<td><s:property value="cellNo"/></td>
		<td><s:property value="birthDate"/></td>
		<td><a href="<s:property value="website"/>">Sitio web</a></td>
		<td><a href="delete?id=<s:property value="id"/>">Borrar</a></td>
	</tr>	
</s:iterator>
</table>
</body>
</html>
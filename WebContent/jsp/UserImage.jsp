<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Cargar imagen de usuario</title>
</head>
<body>
	<h2>Ejemplo para cargar imagen</h2>
	<s:actionerror />
	<s:form action="/userImage.action" method="post" enctype="multipart/form-data">
		<s:file name="userImage" label="Imagen de usuario" />
		<s:submit value="Cargar" align="center" />
	</s:form>
</body>
</html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%><html>
<head>
<title>Éxitoso: Carga de imágen de usuario</title>
</head>
<body>
	<h2>Ejemplo para cargar imágen</h2>
	Imágen de usuario:
	<s:property value="userImage" />
	<br />Tipo de contenido:
	<s:property value="userImageContentType" />
	<br />Nombre de archivo:
	<s:property value="userImageFileName" />
	<br />Imágen cargada:
	<br />
	<img src="<s:property value="userImageFileName"/>" />
</body>
</html>
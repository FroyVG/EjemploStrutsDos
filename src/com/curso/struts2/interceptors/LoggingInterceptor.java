package com.curso.struts2.interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LoggingInterceptor implements Interceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8184492669140193277L;

	@Override
	public void destroy() {
		System.out.println("Destruyendo LoggingInterceptor...");
	}

	@Override
	public void init() {
		System.out.println("Inicializando LoggingInterceptor...");
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		String className = invocation.getAction().getClass().getName();
		long startTime = System.currentTimeMillis();

		System.out.println("Antes de llamar la acci�n: " + className);
		String result = invocation.invoke();
		long endTime = System.currentTimeMillis();
		System.out.println("Despu�s de llamar la acci�n: " + className
				+ " Tiempo tomado: " + (endTime - startTime) + " ms");
		return result;
	}

}